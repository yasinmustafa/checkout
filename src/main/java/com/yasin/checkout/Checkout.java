package com.yasin.checkout;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Predicate;

public class Checkout {
	
	public static final String APPLE = "Apple";
	public static final String ORANGE = "Orange";
	public static final Double APPLE_PRICE = 0.6;
	public static final Double ORANGE_PRICE = 0.25;

	public static Double outputTotalCost(List<String> shopping) {
		
		Predicate <String> applesOnly = Predicate.isEqual(APPLE);
		Predicate <String> orangesOnly = Predicate.isEqual(ORANGE);
		
		Long noOfApples = shopping.stream()
							.filter(applesOnly).count();
		
		Long noOfOranges = shopping.stream()
				.filter(orangesOnly).count();
		
		Long offerApples = noOfApples/2;
		Long remainderApples = noOfApples%2;
		Long appleCountAfterOffer = offerApples + remainderApples;  
		
		Long offerOranges = (noOfOranges/3) * 2;
		Long remainderOranges = noOfOranges%3;
		Long orangeCountAfterOffer = offerOranges + remainderOranges;
		
		BigDecimal totalCostOfApples = calculateTotalForItem(appleCountAfterOffer, APPLE_PRICE);
		BigDecimal totalCosOfOranges = calculateTotalForItem(orangeCountAfterOffer, ORANGE_PRICE);

		return totalCostOfApples.add(totalCosOfOranges).doubleValue();
		
	}
	
	protected static BigDecimal calculateTotalForItem(Long itemCount, Double itemPrice) {
		if (itemCount.intValue()==0) {
			return BigDecimal.ZERO;
		}
		return BigDecimal.valueOf(itemCount).multiply(new BigDecimal(itemPrice.toString()));
		
	}
}
