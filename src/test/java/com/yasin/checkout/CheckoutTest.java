package com.yasin.checkout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CheckoutTest {
	
	@Test
	public void shouldBeCorrectPriceForApple() {
		Assert.assertTrue(Checkout.APPLE_PRICE==0.6);
	}
	
	@Test
	public void shouldBeCorrectPriceForOrange() {
		Assert.assertTrue(Checkout.ORANGE_PRICE==0.25);
	}
	
	@Test
	public void shouldCheckoutThreeApplesAndOneOrange() {
		List <String> shopping = Arrays.asList("Apple", "Apple", "Apple", "Orange");
		Assert.assertTrue(Checkout.outputTotalCost(shopping) == 1.45);
	}
	
	@Test
	public void shouldCheckouTwoApplesAndThreeOranges() {
		List <String> shopping = Arrays.asList("Apple", "Orange", "Apple", "Orange", "Orange");
		Assert.assertTrue(Checkout.outputTotalCost(shopping) == 1.10);
	}
	
	@Test
	public void shouldCheckoutNoApples() {
		List <String> shopping = Arrays.asList("Orange", "Orange");
		Assert.assertTrue(Checkout.outputTotalCost(shopping) == 0.5);
	}
	
	@Test
	public void shouldCheckoutNoOranges() {
		List <String> shopping = Arrays.asList("Apple", "Apple");
		Assert.assertTrue(Checkout.outputTotalCost(shopping) == 0.6);
	}
	
	@Test
	public void shouldCheckoutNothing() {
		List <String> shopping = new ArrayList<>();
		Assert.assertTrue(Checkout.outputTotalCost(shopping) == 0);
	}

}
